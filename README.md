![Image of Max ISiS](https://forum.ircam.fr/media/uploads/images/Projects/maxisis_front_v1.2.8.png)

Max GUI for ISiS singing synthesis software.

> - **Author**: [Greg Beller](https://www.ircam.fr/person/gregory-beller/), [Julien Pallière](https://www.linkedin.com/in/julienpalliere), [Grégoire Lorieux](https://www.ircam.fr/person/-4/)
> - **Date:** 02-02-2021
> - **Version 1.2.8c**

## System Requirements ##
* MacOSX
* [Max8](https://forum.ircam.fr/projects/detail/max-8/)
* Live10 for ISiS4Live M4L device

## Dependencies ##
This patch uses 

* [ISiS](https://forum.ircam.fr/projects/detail/isis/) V1.2.8 
* [Spat5](https://forum.ircam.fr/projects/detail/spat/) 
* [bach](https://www.bachproject.net/) V0.8.1 or see package manager [shell 1.0b2](https://github.com/jeremybernstein/shell/releases) (thanks Jeremy Bernstein)

**Notes**

First drag your isis.sh script in the Max patch. Then select or drag a valid XML file (om.xml for instance) Edit in place the score, change labels, then press ISiS it !... Watch the folder of your XML file, 3 files have been created 

* a cfg file: ISiS score translation of the XML file 
* a log file: ISiS console log 
* a wav file: with date and hour added

> **Credits** 
>
> Thanks to [Axel Röbel](https://www.ircam.fr/person/axel-roebel/) and [Luc Ardaillon](https://www.ircam.fr/person/luc-ardaillon/) for the dev of [ISiS](https://forum.ircam.fr/projects/detail/isis/). This device is being using the Ircam Forum Tempalte M4L device.
